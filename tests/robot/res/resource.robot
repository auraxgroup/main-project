*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library

*** Variables ***
${SERVER}         staging-auraxgroup.herokuapp.com
${BROWSER}        Firefox
${DELAY}          1
${VALID USER}     test
${VALID PASSWORD}    test
${BASIC URL}         https://${SERVER}/
${YHTEENVETO URL}    https://${SERVER}/index.php
${ARVIOINTI URL}      https://${SERVER}/arviointi.php

*** Keywords ***
Open Browser To Basic Page
    Open Browser    ${BASIC URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}

Open Browser To Yhteenveto Page
    Open Browser    ${YHTEENVETO URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Yhteenveto Page Should Be Open
    
Open Browser To Arvionti Page
    Open Browser    ${ARVIOINTI URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Arviointi Page Should Be Open

Go To Yhteenveto Page
    Go To    ${YHTEENVETO URL}
    Yhteenveto Page Should Be Open

Go To Arviointi Page
    Go To    ${ARVIOINTI URL}
    Arviointi Page Should Be Open

Input Username
    [Arguments]    ${username}
    Input Text    uid    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    passwd    ${password}

Submit Credentials
    Click Button    login_button

Yhteenveto Page Should Be Open
    Location Should Be    ${YHTEENVETO URL}
    Title Should Be    Yhteenveto

Arviointi Page Should Be Open
    Location Should Be    ${ARVIOINTI URL}
    Title Should Be    Arviointi
