*** Settings ***
Documentation     Test navigation to arviointi page
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          res/resource.robot

*** Test Cases ***
Navigate to arviointi
    Open Browser To Yhteenveto Page
    Click Button    GoToArviointi
    Arviointi Page Should Be Open
    [Teardown]    Close Browser
