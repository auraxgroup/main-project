*** Settings ***
Documentation     Yhteenveto page should be landing page
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          res/resource.robot

*** Test Cases ***
Navigate to yhteenveto page
    Open Browser To Basic Page
    Title Should Be    Yhteenveto
    [Teardown]    Close Browser
