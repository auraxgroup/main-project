*** Settings ***
Documentation     Submitting empty arvionti form.
...
...               These tests are data-driven by their nature. They use a single
...               keyword, specified with Test Template setting, that is called
...               with different arguments to cover different scenarios.
...
...               This suite also demonstrates using setups and teardowns in
...               different levels.
Suite Teardown    Close Browser
Resource          res/resource.robot
*** Test Cases ***
Submit Empty Form
    Open Browser To Arvionti Page
    Submit Arviointi