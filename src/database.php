<?php

require __DIR__ . '/../vendor/autoload.php';

// Test code
if(isset($_GET['test'])) {
    $db = new Database();
    echo $db->error;
    echo "works";
}
// End Test code

class Database
{
    public $error;
    private $dbconn;

    public function __construct() {
        $dbopts = parse_url(getenv('DATABASE_URL'));
        $dbname = ltrim($dbopts["path"],'/');
        $dbhost = $dbopts["host"];
        $dbport = $dbopts["port"];
        $dbuser = $dbopts["user"];
        $dbpass = $dbopts["pass"];
        $this->dbconn = pg_connect("host={$dbhost} port={$dbport} dbname={$dbname} user={$dbuser} password={$dbpass}")
                                   or die('Could not connect: ' . pg_last_error());

        $this->initDatabase();
    }

    public function __destruct() {
         pg_close($this->dbconn);
    }

    /*public function queryStudents($string) {
    // Query db
    $query = pg_prepare($this->$dbconn, "student_query", "SELECT student.name, student.tunnus FROM students WHERE name=$1");
    $result = pg_execute($this->$dbconn, "student_query", array($string));

    // Results to array
    $arr = array();
    while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
    $line = array();
    foreach ($line as $col_value) {
    array_push($line, $col_value);
    }
    array_push($arr, $line);
    }

    // to JSON?
    return $arr;
    }*/

    private function addAnswer($key, $arvosana, $lausunto, $kommentti="") {
        $query = pg_prepare($this->dbconn, "add_answer", "INSERT INTO arviointi(arvosana, lausunto, kommentti) VALUES ($1, $2, $3) RETURNING avain");
        $result = pg_execute($this->dbconn, "add_answer", array($arvosana, $lausunto, $kommentti));
        // TODO: Hanki avain resultista
        $avain = 0;

        // TESTI
        $line = pg_fetch_array($result, null, PGSQL_ASSOC);
        $avain = $line[0];

        $query = pg_prepare($this->dbconn, "update_answer", "UPDATE arvio SET arviointi_avain=$1 WHERE tunnus = $2");
        $result = pg_execute($this->dbconn,"update_answer", array($avain, $key));
    }

    private function addArviointi($henkiloId, $opinnayteId, $key) {
           $query = pg_prepare($this->dbconn, "add_arviointi", "INSERT INTO arvio(henkilo_avain, opinnayte_avain, tunnus) VALUES ($1, $2, $3)");
           $result = pg_execute($this->dbconn, "add_arviointi", array($henkiloId, $opinnayteId, $key));
    }

    // id = oppilaan id
    private function getArvioinnit($id) {
        $query = pg_prepare($this->dbconn, "get_arviointi", "SELECT arvio.arviointi_avain, henkilo.avain henkilo.sposti, arviointi.arvosana, arviointi.lausunto, arviointi.kommentti
        opinnayte.avain, opinnayte.otsikko, opinnayte.valmispvm
        FROM arvio
        JOIN arviointi ON arvio.arviointi_avain = arviointi.avain
        JOIN henkilot ON arvio.arviointi_avain = henkilo.avain
        JOIN opinnayte ON arvio.arviointi_avain = opinnayte.avain
        WHERE arvio.opinnayte_avain = $1");
        $result = pg_execute($this->dbconn, "get_arviointi", array($id));
        return $result;
    }

    public function getStudents($id) {
        $query = pg_prepare($this->dbconn, "get_students", "SELECT * FROM henkilot WHERE tunnus = $1");
        $result = pg_execute($this->$dbconn, "student_query", array($id));
        return $result;
    }

    public function getAllStudents(){
        //hakee henkilotaulusta kaikki, joiden rooli on roolitaulusta "0"
        $query = pg_prepare($this->dbconn, "get_all_students", "SELECT * FROM henkilot WHERE rooli_avain = (SELECT avain FROM rooli WHERE tunnus = 0)");
        $result = pg_execute($this->$dbconn, "student_query");
        return $result;
    }

    public function addOpinnayte($otsikko, $pvm) {
        $query = pg_prepare($this->dbconn, "add_opinnaytetyo", "INSERT INTO opinnayte(otsikko, valmispvm) VALUES ($1, $2)");
        pg_execute($this->dbconn, "add_opinnaytetyo", array($otsikko, $pvm));
    }

    public function addStudent($tunnus, $nimi, $sposti) {
        addPerson(0, $sposti, $nimi, $tunnus);
    }

    private function addPerson($rooli, $sposti, $nimi="", $tunnus="") {
        $query = "INSERT INTO henkilo (nimi, sposti, tunnus, rooli_avain) VALUES ($1, $2, $3, $4)";
        $result = pg_execute($this->dbconn, "add_preson", array($nimi, $sposti, $tunnus, $rooli));
    }

    private function initDatabase() {
        //https://dba.stackexchange.com/questions/68266/what-is-the-best-way-to-store-an-email-address-in-postgresql --- EMAIL
        $query = "
            CREATE TABLE IF NOT EXISTS arviointi(
                avain       SERIAL     PRIMARY KEY UNIQUE,
                arvosana    INT     NOT NULL,
                lausunto    TEXT    NOT NULL,
                kommentti   TEXT
            );

           CREATE TABLE IF NOT EXISTS rooli(
                avain       SERIAL     PRIMARY KEY UNIQUE,
                tunnus       INT     NOT NULL UNIQUE,
                typename    TEXT    NOT NULL
            );
           INSERT INTO rooli(tunnus, typename) VALUES (0, 'Oppilas') ON CONFLICT (tunnus) DO NOTHING;
           INSERT INTO rooli(tunnus, typename) VALUES (1, 'Ohjaaja') ON CONFLICT (tunnus) DO NOTHING;
           INSERT INTO rooli(tunnus, typename) VALUES (2, 'Toimeksiantaja') ON CONFLICT (tunnus) DO NOTHING;

           CREATE TABLE IF NOT EXISTS henkilo(
                avain       SERIAL     PRIMARY KEY UNIQUE,
                nimi       TEXT,
                sposti      TEXT    NOT NULL UNIQUE,
                tunnus      TEXT    UNIQUE,
                rooli_avain INT     NOT NULL,

                FOREIGN KEY (rooli_avain) REFERENCES rooli (avain)
            );

           CREATE TABLE IF NOT EXISTS opinnayte(
                avain       SERIAL     PRIMARY KEY UNIQUE,
                otsikko     TEXT    NOT NULL,
                valmispvm   DATE    NOT NULL
           );

            CREATE TABLE IF NOT EXISTS arvio(
                avain                   SERIAL     PRIMARY KEY,
                arviointi_avain         INT,
                henkilo_avain           INT     NOT NULL,
                opinnayte_avain         INT     NOT NULL,
                tunnus                  TEXT    NOT NULL UNIQUE,

                FOREIGN KEY (arviointi_avain) REFERENCES arviointi (avain),
                FOREIGN KEY (henkilo_avain) REFERENCES henkilo (avain),
                FOREIGN KEY (opinnayte_avain) REFERENCES opinnayte (avain)
            );
        ";
        $this->error = pg_query($query) or die('Query failed: ' . pg_last_error());
    }
}
