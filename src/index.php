<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Yhteenveto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/skeleton.css">
</head>
<body>

    <div class="row navbar">
        <img src="images/logo.png">
    </div>

    <div class="container">
        <div class="sidebar three columns" >
            <a href="newStudent.php"><button id="addButton">Uusi</button></a>
            <div id="studList"></div>
        </div>

        <div class="info seven columns">
        
            <div class="row" id="studentInfo">
                <h2>Valitse oppilas!</h2>
            </div>

            <hr>
            <h1>Arvioinnit</h1>
            <div class="row" style="background-color:rgba(40,105,138,.5)">
                Anna oma arviointi <a href="arviointi.php" name="GoToArviointi"><button>Arviointi</button></a>
            </div>
            <br>
            <div class="row" style="border: 1px solid black">
                <h4>Ohjaaja 2</h4>
                <form method="post" action="send.php">
                    <input type="email" name="email" id="email" placeholder="Sähköposti" required>
                    <input type="submit" value="Lähetä linkki">
                </form>
                <!--button>Lisää...</button-->
            </div>
            <br>
            <div class="row" style="border: 1px solid black">
                <h4>Opiskelijan itse arviointi</h4>
                <form method="post" action="send.php">
                    <input type="email" name="email" id="email"  placeholder="Sähköposti" required>
                    <input type="submit" value="Lähetä linkki">
                </form>
                <!--button>Lisää...</button-->
            </div>
            <br>
            <div class="row" style="border: 1px solid black">
                <h4>Vertais oppilas</h4>
                <form method="post" action="send.php">
                    <input type="email" name="email" id="email"  placeholder="Sähköposti" required>
                    <input type="submit" value="Lähetä linkki">
                </form>
                <!--button>Lisää...</button-->
            </div>
            <br>
            <div class="row" style="border: 1px solid black">
                <h4>Toimeksianta</h4>
                <form method="post" action="send.php">
                    <input type="email" name="email" id="email"  placeholder="Sähköposti" required>
                    <input type="submit" value="Lähetä linkki">
                </form>
                <!--button>Lisää...</button-->
            </div>
        </div>
    </div>
    <script>

      var studentjson;
    getList();


function studList(json) {
    var output="";

    //for(var i =0; i < json.count; i++) {
    for (var i in json) {

        output += "<div id='studentBox' style='border: 1px solid black' onclick='studentInfo("+i+");'><strong>";
        output += json[i].tunnus;
        output += "</strong><br>";
        output += json[i].name;
        output += "</div>";
    }
    document.getElementById("studList").innerHTML = output;
}

function getList() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
            if (xmlhttp.status == 200) {
                var json = JSON.parse(xmlhttp.responseText);
                studentjson = json;
                studList(json);
                return json;

            }
            else if (xmlhttp.status == 400) {
                alert('There was an error 400');
            }
            else {
                alert('something else other than 200 was returned');
            }
        }
    };
    xmlhttp.open("GET", "devInfo.json", true);
    xmlhttp.send();
}

function studentInfo(id){
            document.getElementById("studentInfo").innerHTML='<table>'
            +'<strong>Opiskelija</strong><tr><td>Opiskelija tunnus:</td>'
              +'<td>'+studentjson[id].tunnus+'</td></tr><tr><td>Opiskelija nimi:</td>'
              +'<td>'+studentjson[id].name+'</td></tr><tr><td>Opiskelija Sähköposti:</td>'
              +'<td>'+studentjson[id].mail+'</td></tr></table><table><strong>Opinnayte </strong>'
              +'<tr><td>Nimi:</td><td>'+studentjson[id].opinnayte+'</td></tr><tr>'
              +'<td>Valmistumis päivä</td><td>'+studentjson[id].valmistuminen+'</td></tr>'
              +'</table>';
}

function arvostelut(id) {
    
}
    </script>
</body>
</html>
