<?php
require '/../vendor/autoload.php';
use Nette\Mail\Message;

$url = "http://"
     . $_SERVER['HTTP_HOST']
     . dirname($_SERVER['PHP_SELF'])
     . '/arviointi.php?';

$url = $url . "key=" . hash("sha256",$_POST['email'].generateRandomString(256));

// TODO: Put key to database with same email

echo $url;


function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ¤%&€$?-_';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function TMPSEND() {
    function mailf($nimi, $phone, $email, $message)
{
    date_default_timezone_set('Europe/Helsinki');

    $mail = new Message;

    $mail->setFrom('JAMK opinnayte järjestelmä <lähettäjä@sposti.asdf>')
         ->addTo('kellekkä@lähetetään.com')
         ->setSubject('Otsikko')
         ->setBody('Viesti. LINKKI');

    // smtp server
    $mailer = new Nette\Mail\SmtpMailer([
        'host' => 'url.com.asdf',
        'username' => 'user',
        'password' => 'pass',
        'secure' => 'ssl'
        /*  'context' =>  [
           'ssl' => [
           'capath' => '/path/to/my/trusted/ca/folder',
           ],
           ],*/
    ]);
    $mailer->send($mail);

    header('Location: http://' . $_SERVER['HTTP_HOST']
         . dirname($_SERVER['PHP_SELF']) . '/'
         . 'index.html');
    die();
}
}