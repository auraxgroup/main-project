<?php
require "database.php";

if(isset($_GET['add'])) {
    $db = new Database;
    echo "WIP";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Arviointi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/skeleton.css">
    </head>

    <body>
        <div class="row navbar">
            <img src="images/logo.png">
        </div>

		<form id="lausunto" method="post" action="arviointi.php?add">
			<table>
				<tr>
					<th></th>
					<th>0</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
				</tr>
				<?php
				$file=file_get_contents( "tiedot.json");
				$json=json_decode($file, true);
				foreach($json as $title=> $arr1) {
					echo "<tr><td>".$title."</td></tr>";
					foreach($arr1 as $subtitle => $statements) {
						echo "<tr><td>".$subtitle;

						foreach($statements as $key => $text) {
							echo "<td class='tab'><input type='radio' id='".$subtitle.$key."' name='".$subtitle."' value='".$key."' onclick='showChecked()' required>"
								."<label for='".$subtitle.$key."'>".$text."</label></td>";
						}
						echo "</td></tr>";
					}
				}
				?>
			</table>
			</div>
			<div class="container">
				<div class="row">
					<div class="eight columns">
						<h3>Vapaat kommentit</h3>
						<textarea form="lausunto" placeholder="Kommentit"></textarea>
					</div>
					<div class="four columns">
						<!--label for="keskiarvo">Arvionnin Kokonaisarvo</label>
						<input class="u-full-width" type="text" placeholder="Kokonaisarvo" id="kokonaisarvo"-->
						<label for="keskiarvo">Arvionnin keskiarvo</label>
						<input class="u-full-width" type="text" placeholder="Keskiarvo" id="keskiarvo" disabled>
					</div>
				</div>
			</div>
			<div id="SubBut" align="center">
				<input type="reset" onclick="return confirm('Haluatko varmasti nollata?')" value="Nollaa">
				<input type="submit" name="submit" value="Lähetä">
			</div>
		</form>
		<script>

			function showChecked(){
				
				var inputElements = document.getElementsByTagName('input');
				var checkedElements = [];
				var totalValue = 0;
				var average = 0;
				
				for(var i = 0; i<inputElements.length; i++){
					
					if(inputElements[i].getAttribute('type')=='radio' && inputElements[i].checked){
						
						checkedElements.push(inputElements[i]);
					}
				}
				
				for(var j = 0; j < checkedElements.length; j++){
					
						totalValue += parseInt(checkedElements[j].value);
						//document.getElementById('kokonaisarvo').value = totalValue;
						document.getElementById('keskiarvo').value = totalValue/16;
					}
				
			}
		
		</script>
	</body>
</html>
