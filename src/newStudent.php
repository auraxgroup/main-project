<?php
require "database.php";

    if(isset($_GET['add'])) {
        $db = new Database();
        $db->addStudent($_POST['id'], $_POST['name'], $_POST['email']);
        $db->addOpinnayte($_POST['thesis'], $_POST['date']);
        echo "<h1>Student Added</h1>";
    }
?>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/skeleton.css">
    </head>
<body>
<form method="POST" action="newStudent.php?add">
Tunnus: <input type="text" name="id" required><br>
Nimi: <input type="text" name="name" required><br>
Sähköposti: <input type="email" name="email" required>
<hr>
Opinnäyte nimi: <input type="text" name="thesis" required><br>
Valmistumis päivä <input type="text" name="date" required><br>
<input type="submit" value="lisää">
</form>
</body>
</html>